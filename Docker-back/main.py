from flask import Flask, request, jsonify, redirect, url_for, abort
from flask_restful import Resource, Api
from sqlalchemy import create_engine
from calculator.simple import SimpleCalculator

e = create_engine('sqlite:///dbCalcul.db')
app = Flask(__name__)

def calculationFunction(calcul):
	c = SimpleCalculator()
	c.run(str(calcul))
	a = c.log
	res = a[-1].replace('result: ', '')
	return res

@app.route('/api/computations/', methods=['POST'])
def post():
	if not request.json or not 'vals' in request.json or not 'op' in request.json:
		abort(400)
	calcul = request.json['vals'][0] + ' ' + request.json['op'] + ' ' + request.json['vals'][1]
	try:
		res = calculationFunction(calcul)
		conn = e.connect()
		lastIds = conn.execute("SELECT MAX(id) FROM RESULTS;").fetchall()
		lastId = int(lastIds[0][0])+1
		queryStr = "INSERT INTO RESULTS VALUES(" + str(lastId) + ", '" + calcul + "', 'completed', " + res + ")"
		query = conn.execute(queryStr)
		result = {'Location ': '/api/operations/'+ str(lastId)}
	except Exception as ex:
		print(ex)
		abort(500)
	return result, 202

@app.route('/api/computations/<int:computation_id>', methods=['GET'])
def get(computation_id):
	result = {'data': []}
	if isinstance(computation_id, int):
		try:
			conn = e.connect()
			query = conn.execute("SELECT id, result from RESULTS where ID='%s';" % computation_id)
			result = {'data': [dict(zip(tuple (query.keys()) ,i)) for i in query.cursor]}
		except Exception as ex:
			print(ex)
			abort(500)
	else:
		abort(400)
	return result

@app.route('/api/operations/<int:operation_id>', methods=['GET'])
def getOp(operation_id):
	result = {'data': []}
	if isinstance(operation_id, int):
		try:
			conn = e.connect()
			query = conn.execute("SELECT id, status from RESULTS where ID='%s';" % operation_id)
			result = {'data': [dict(zip(tuple (query.keys()) ,i)) for i in query.cursor]}
		except Exception as ex:
			print(ex)
			abort(500)
	else:
		abort(400)
	return result

@app.route('/api/all', methods=['GET'])
def getAll():
	result = {'data': []}
	try:
		conn = e.connect()
		query = conn.execute("SELECT * FROM RESULTS;")
		result = {'data': [dict(zip(tuple (query.keys()) ,i)) for i in query.cursor]}
	except Exception as ex:
		print(ex)
		abort(500)
	return result

app.run(debug=True)
