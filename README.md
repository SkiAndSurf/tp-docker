# TP-Docker

TP Docker - ZZ3

JSON POST example with curl :
curl -v -X POST -H "Content-Type: application/json" -d '{"vals": ["1","1"], "op": "+"}' http://127.0.0.1:5000/api/computations/

GET example with curl:
curl http://127.0.0.1:5000/api/operations/0

## How to build and run
docker build --tag python-docker .

docker run --publish 5000:5000 python-docker

## How to build and run Front

docker build -t docker-front .
docker run -dit --name my-running-app -p 8080:80 docker-front
