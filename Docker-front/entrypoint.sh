#!/bin/bash
set -e

# Remove a potentially pre-existing server.pid for Rails.
cp /myapp/src/calculette.* /var/www/html/
rm -rf /var/www/html/index.html
mv /var/www/html/calculette.html /var/www/html/index.html

# Then exec the container's main process (what's set as CMD in the Dockerfile).
exec "$@"