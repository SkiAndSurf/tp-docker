const buttons = document.getElementsByTagName('button');
const calcul = document.getElementById("calcul");
const id = document.getElementById("ID");
const res = document.getElementById("res");
const screen = document.getElementById("screen");

var requestPOST = new XMLHttpRequest();
var requestGET = new XMLHttpRequest();
var requestGETRes = new XMLHttpRequest();

var ip = "http://localhost:5000/api/";

requestGET.open("GET", ip + "all");
requestGET.send();

for (let i = 0; i < buttons.length; i++){
    buttons[i].addEventListener('click', function(){
        switch(buttons[i].innerHTML){
            case ' AC ':
                console.log("clear");
                screen.innerHTML = "";
                break;
            case ' = ':
                console.log("calcul : " + parse(screen.innerHTML).vals + " " + parse(screen.innerHTML).op);
                requestPOST.open("POST", ip + "computations/");
                requestPOST.setRequestHeader("Content-Type", "application/json");
                requestPOST.send(JSON.stringify(parse(screen.innerHTML)));
                screen.innerHTML = "";
                break;
            case 'send':
		console.log(id.value);
                if(id.value !=""){
                    requestGETRes.open("GET", ip + "computations/" + id.value);
                    requestGETRes.send();
                }
                break;
            default:
                console.log("buttons " + buttons[i].innerHTML);
                screen.innerHTML = screen.innerHTML + buttons[i].innerHTML;
        }
    });
}

requestPOST.onreadystatechange = function(){
    if(this.readyState == XMLHttpRequest.DONE && this.status == 202){
        requestGET.open("GET", ip + "all");
        requestGET.send();
    }
}

requestGET.onreadystatechange = function(){
    if(this.readyState == XMLHttpRequest.DONE && this.status == 200){
        calcul.innerHTML = "";
        var response = JSON.parse(this.responseText).data;
        for(let i=0; i < response.length ; i++){	
            calcul.innerHTML += "id : " + response[i].id + " " + response[i].calculation + " " + response[i].status + "<br>";
        }
    }
}

requestGETRes.onreadystatechange = function(){
    if(this.readyState == XMLHttpRequest.DONE && this.status == 200){
        var request = JSON.parse(this.responseText).data;
        res.innerHTML = " Resultat : " + request[0].result;
    }
}

function parse(val){
    let list = val.split(" ");
    let res = {
        vals: [],
        op: ""
    };
    res.vals.push(list[0]);
    res.vals.push(list[2]);
    res.op = list[1];
    return res;
}
